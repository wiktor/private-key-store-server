{ lib
, python3
, qt6
, src
, version
}:

python3.pkgs.buildPythonApplication rec {
  pname = "pks";
  inherit src version;
  pyproject = true;

  nativeBuildInputs = [
    qt6.wrapQtAppsHook
  ];
  buildInputs = [
    qt6.qtbase
    qt6.qtwayland
  ];

  propagatedBuildInputs = with python3.pkgs; [
    setuptools
    pyside6
    pysequoia
  ];

  # Prevent double wrapping
  dontWrapQtApps = true;
  preFixup = ''
    makeWrapperArgs+=("''${qtWrapperArgs[@]}")
  '';

  meta = {
    description = "Provides GUI frontend for private key operations";
    homepage = "https://codeberg.org/wiktor/private-key-store-server";
    license = lib.licenses.asl20;
  };
}
