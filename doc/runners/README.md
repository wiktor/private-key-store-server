# Runners

Building cross-platform binaries requires custom runners.

Fortunately Codeberg.org which hosts Forgejo.org allows attaching custom runners:

![runners config](runners.png)

## Setting up

  - Grab the correct binary from https://dl.gitea.com/act_runner/
  - Retrieve the registration token from https://codeberg.org/user/settings/actions/runners
  - Register the runner by: `act_runner register`. It is important that the runner's label ands with `:host`. Using that label means the runner will execute builds using host machine instead of docker. For example in my case I'm using `macos-latest:host` and `windows-latest:host` labels. (See https://gitea.com/gitea/act_runner/issues/179 for more details)
  - Dump the configuration with `act_runner generate-config > config.yaml`
  - Start the runner: `act_runner daemon`
  - The runner should be visible as `Idle` in Codeberg.org UI.

Some machines can be set up [using VMs](https://github.com/wiktor-k/vms).

## Windows

Unfortunately `act_runner` uses an old version of `act` which means
the built in `cmd.exe` shell [will not
work](https://github.com/nektos/act/issues/1891#issuecomment-1718165513). The easiest workaround for the time being is using PowerShell in the job:

```yaml
jobs:
  test-actions-on-win-powershell:
    runs-on: windows-latest
    steps:
      - run: Get-ComputerInfo WindowsVersion
      - uses: actions/checkout@v3
      - run: Get-Content README.md | Measure-Object -Line
    defaults: # This needs to be set for all windows jobs
      run:
        shell: powershell
```

The PowerShell needs appropriate security settings to run unauthorized scripts: https://stackoverflow.com/a/64633728

Additionally using actions requires Node.JS runtime which can be installed using `winget install OpenJS.NodeJS`

### SSH

Windows can be configured to expose SSH server.

From an administrator's PowerShell session execute:

```
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
```

If the startup of this service should be automatic:

```
Set-Service -Name sshd -StartupType Automatic
```

And finally start the service:

```
Start-Service sshd
```

More elaborate description with GUI examples and details is at https://theitbros.com/ssh-into-windows/

### Shutdown

Execute:

```
shutdown /s /f /t 0
```

([Source](https://www.howtogeek.com/512012/how-to-shut-down-your-windows-10-pc-using-command-prompt/))

## macOS

macOS runners do not need any additional configuration.

### SSH

SSH server can be enabled on a macOS machine using *Remote Login*
option under *Sharing* in *System Preferences*. See more details at
https://superuser.com/a/105937

### Shutdown

Execute:

```
sudo shutdown -h now
```
