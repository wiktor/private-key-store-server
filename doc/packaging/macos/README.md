# macOS packaging

macOS packaging requires the use of a Code Signing certificate.

For this project we decided to follow along Apple's suggestions:
entering [Apple Developer](https://developer.apple.com/developer-id/)
program which costs 99 USD yearly and requires verification of the ID
document via online scanning service.

See [briefcase description](https://briefcase.readthedocs.io/en/latest/how-to/code-signing/macOS.html) for more details.

To obtain the code signing certificate identity use the following command:

```sh
$ security find-identity -p basic -v
  1) 85E488CBEAFB5A948660190F28AB5A6B1C787D5B "Developer ID Application: WIKTOR KWAPISIEWICZ (AABBCCDDEE)"
     1 valid identities found
```

The `85E488CBEAFB5A948660190F28AB5A6B1C787D5B` is the certificate
identity while `AABBCCDDEE` is the team ID (sometimes required during
troubleshooting).

## Notarization

It's advisable to notarize applications before distribution so that
they can be ran even on an offline systems.

Some tools such as `briefcase` automatically notarize applications
after they are signed. If that fails or one wants to notarize as a
separate process one needs to use `notarytool` for this:

First, store credentials (this should be done only once):

```sh
xcrun notarytool store-credentials --apple-id test@example.com --team-id AABBCCDDEE
```

Then submit the binary using key chain profile defined in a previous step:

```sh
xcrun notarytool submit dist/pks-2.0.0.dmg --keychain-profile NOTARY --wait
```

The `--wait` flag will make the tool periodically check for a status
change. Note that for the first time this can literally take hours to
complete.

If the notarization fails one can print the reason with the `log` subcommand:

```sh
xcrun notarytool log 7d9249fb-f9ac-4bb1-b296-256ebabff66b --apple-id test@example.com --team-id AABBCCDDEE
```

One needs to use the submission ID that is returned when `submit`
command returns (here it's `7d9249fb-f9ac-4bb1-b296-256ebabff66b`).
