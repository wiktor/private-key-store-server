# Windows Packaging

Windows packaging requires the use of Code Signing certificate.

For this project we decided to use [Open Source Code Signing][OSCS] which costs 69 EUR gross and includes a smartcard and a smartcard reader:

![smartcard reader](token.jpg)

Do note that getting the code signing certificate, which is valid for a year, requires verification of the ID document either via automated system which scans the ID or via visiting a partner office. The latter is additionally paid and some partners do not inform about this additional fee in advance.

[OSCS]: https://shop.certum.eu/open-source-code-signing.html

## Certificate

The code signing certificate can be installed through the GUI:

![code signing certificate](cert.png)

By default the cerificate will be put in the `Personal` store:

![personal cert store](certmgr.png)

The certificate is identified by `Thumbprint` which needs to be set during code signing:

![certificate thumbprint](thumbprint.png)
