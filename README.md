# Private Key Store Server

GUI frontend for private key operations.

This program let's you use your private keys to sign and decrypt data
as well as set notations that are used by the
[Keyoxide](https://keyoxide.org/) project.

One type of Keyoxide notation is supported more tightly - [binding your
Forgejo / Gitea / Codeberg account][GITEA] to your OpenPGP key.  This can be
achieved by an easy to follow wizard.

[GITEA]: https://wiktor.codeberg.page/private-key-store-server-docs/04-proof.html

See [documentation][DOCS] for more details.

[DOCS]: https://wiktor.codeberg.page/private-key-store-server-docs/

![main window](doc/main.png)

## Installation

The software requires two python libraries: PySide6 for UI and
pysequoia for providing OpenPGP operations. Both can be installed from
the requirements file:

```sh
pip install -r requirements.txt
```

Note that, depending on your system, PySequoia may require Rust
toolchain to be built. [In the future][#79] more platforms with
prebuilt binaries will be supported.

[#79]: https://codeberg.org/wiktor/pysequoia/issues/79


## Running

### Nix

To run the main UI using NixOS flakes:

```sh
nix run "git+https://codeberg.org/wiktor/private-key-store-server"
```

### Development

Starting the UI:

```sh
briefcase dev
```

If you need a key for testing you can create it using [`sq`][SQ]:

```sh
sq key generate --userid 'Test <test@example.com>' --export key.asc
```

If you want to complete the proof verification step you need to use your real e-mail address as it is verified by https://keys.openpgp.org/

[SQ]: https://crates.io/crates/sequoia-sq/

## Testing

This project can be tested in standard way using `pytest`. A composite
script that runs all lints and integration tests is provided at
`scripts/check`.

These packages are required to run the test suite:

```sh
pip install black pytest pytest-qt
```

## License

This project is licensed under [Apache License, Version 2.0][APL].

[APL]: https://www.apache.org/licenses/LICENSE-2.0.htmlq

## Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the package by you shall be under the terms
and conditions of this license, without any additional terms or
conditions.

## Sponsors

My work is being supported by these generous organizations
(alphabetical order):
  - [nlnet.nl](https://nlnet.nl/)
  - [pep.foundation](https://pep.foundation/)
  - [sovereigntechfund.de](https://sovereigntechfund.de/en.html)
