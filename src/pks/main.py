import sys

from PySide6.QtWidgets import (
    QApplication,
    QDialog,
    QMainWindow,
    QPushButton,
    QFileDialog,
    QMessageBox,
    QInputDialog,
    QLineEdit,
    QSystemTrayIcon,
)

from PySide6.QtGui import QIcon

try:
    from PySide6.QtHttpServer import QHttpServer
except ImportError:
    pass

from PySide6.QtNetwork import QHostAddress, QTcpServer
from PySide6.QtCore import QByteArray

from pks.ui.main import Ui_Main
from pks.proof import AddProofWindow

from pysequoia import Cert, Card, sign, decrypt, Notation
from pks.ui.add_forge import Ui_AddForgeProof
from os.path import exists
import webbrowser


def route(request):
    return win.route(request)


def after_request(response, request):
    response.setHeader(b"Server", b"Private Key Server")
    response.setHeader(b"Access-Control-Allow-Origin", b"*")


class Window(QMainWindow):
    """Main window."""

    def __init__(self, parent=None):
        """Initializer."""
        super().__init__(parent)

        self.server = None
        self.file_location = None

        self.ui = Ui_Main()
        self.ui.setupUi(self)
        self.tray_icon = QSystemTrayIcon()
        self.tray_icon.setIcon(QIcon("icon.png"))
        self.tray_icon.setVisible(True)

        self.refresh_cards_clicked(None)

        self.ui.pages.setCurrentIndex(0)
        self.ui.select_file.mousePressEvent = self.select_file_clicked
        self.ui.refresh_cards.mousePressEvent = self.refresh_cards_clicked
        self.ui.web_op_url.mousePressEvent = self.go_to_web_op_clicked

        self.ui.cards.currentIndexChanged.connect(self.cards_changed)

        self.ui.sign_data.clicked.connect(self.sign_data_clicked)
        self.ui.sign_notations.clicked.connect(self.sign_notations_clicked)
        self.ui.decrypt_data.clicked.connect(self.decrypt_data_clicked)
        self.ui.start_listen.clicked.connect(self.start_listen_clicked)

        self.ui.add_forge.clicked.connect(self.add_forge_clicked)

        self.ui.unlock_any.clicked.connect(self.unlock_any_clicked)

        self._set_signer(None)
        self._set_decryptor(None)
        self._set_certifier(None)

    def go_to_web_op_clicked(self, _):
        webbrowser.open(self.ui.web_op_url.text())

    def ask_for_password(self, text):
        text, ok = QInputDialog.getText(None, "Password", text, QLineEdit.Password)
        if not ok:
            return None
        else:
            return text

    def refresh_cards_clicked(self, _):
        self.ui.cards.clear()
        self.ui.cards.addItem("-- None --", None)
        try:
            for card in Card.all():
                self.ui.cards.addItem(f"{card.ident}: {card.cardholder}", card)
        except:
            # No reader found.
            # See: https://codeberg.org/wiktor/pysequoia/issues/6
            pass

    def cards_changed(self, index):
        self.ui.select_file.setText("Select OpenPGP secret file...")
        self.file_location = None

    def add_forge_clicked(self):
        cert = Cert.from_file(self.file_location)
        p = AddProofWindow(cert, self.certifier)
        p.exec()

    def sign_data_clicked(self):
        fileName = QFileDialog.getOpenFileName(
            self, "Open data file", "", "Any file (*)"
        )
        file_to_sign = fileName[0]
        with open(file_to_sign, "rb") as f:
            text_to_sign = f.read()

        self.tray_icon.showMessage("Private Key Store", f"Signing file: {file_to_sign}")
        signed = sign(self.signer, text_to_sign)

        fileName = QFileDialog.getSaveFileName(
            self, "Signed file", "", "OpenPGP armored file (*.asc)"
        )
        signed_file = fileName[0]
        with open(signed_file, "wb") as f:
            f.write(signed)

        dlg = QMessageBox(self)
        dlg.setWindowTitle("Signing")
        dlg.setText("File has been signed!")
        self.on_dialog(dlg)

    def sign_notations_clicked(self):
        cert = Cert.from_file(self.file_location)

        fileName = QFileDialog.getOpenFileName(
            self, "Notations file", "", "Notations file (*.txt)"
        )
        notations_file = fileName[0]
        notations = []
        with open(notations_file, "r") as f:
            for line in f.readlines():
                notation = line.strip().split("=", 1)
                notations.append(Notation(notation[0], notation[1]))

        cert = cert.set_notations(self.certifier, notations)

        fileName = QFileDialog.getSaveFileName(
            self, "Updated certificate", "", "OpenPGP armored file (*.asc)"
        )
        cert_file = fileName[0]
        with open(cert_file, "w") as f:
            f.write(str(cert))

        dlg = QMessageBox(self)
        dlg.setWindowTitle("Signing")
        dlg.setText("Notations have been set!")
        self.on_dialog(dlg)

    def decrypt_data_clicked(self):
        fileName = QFileDialog.getOpenFileName(
            self, "Encrypted file", "", "OpenPGP encrypted file (*.asc *.pgp *.gpg)"
        )
        file_to_decrypt = fileName[0]
        with open(file_to_decrypt, "rb") as f:
            text_to_decrypt = f.read()

        self.tray_icon.showMessage(
            "Private Key Store", f"Decrypting file: {file_to_decrypt}"
        )
        decrypted = decrypt(decryptor=self.decryptor, bytes=text_to_decrypt)

        fileName = QFileDialog.getSaveFileName(
            self, "Open data file", "", "Any file (*)"
        )
        decrypted_file = fileName[0]
        with open(decrypted_file, "w") as f:
            f.write(str(decrypted.bytes))

        dlg = QMessageBox(self)
        dlg.setWindowTitle("Decryption")
        dlg.setText("File has been decrypted!")
        self.on_dialog(dlg)

    def select_file_clicked(self, _):
        fileName = QFileDialog.getOpenFileName(
            self, "Open key", "", "OpenPGP key files (*.asc *.pgp *.gpg)"
        )
        self.file_location = fileName[0]
        self.ui.select_file.setText("OpenPGP secret file: " + fileName[0])
        self.ui.cards.setCurrentIndex(0)

    def unlock_card_clicked(self):
        self._set_signer(None)
        self._set_decryptor(None)
        card = self.ui.cards.currentData()

        dlg = QMessageBox(self)
        dlg.setWindowTitle("Card unlock")
        dlg.setText("Card unlock succeeded!")

        pin = self.ask_for_password(f"PIN for {card.ident}")
        if pin is None:
            return

        try:
            signer = card.signer(pin)
            self._set_signer(signer)
            self._set_certifier(signer)

            # pysequoia 0.1.21 can enumerate card keys
            if hasattr(card, "keys"):
                signing_key = next(key for key in card.keys if "sign" in key.usage)
                if signing_key is not None:
                    self.ui.key_fpr.setText(signing_key.fingerprint)
                    self.ui.web_op_url.setText(
                        self.ui.web_op_url.text() + signing_key.fingerprint
                    )
            self.ui.pages.setCurrentIndex(1)
        except Exception as e:
            dlg.setText("Card unlock failed: " + str(e))
            self.on_dialog(dlg)
            return

        try:
            self._set_decryptor(card.decryptor(pin))
        except Exception as e:
            dlg.setText("Card unlock failed: " + str(e))
            self.on_dialog(dlg)
            return

    def unlock_any_clicked(self):
        self._set_signer(None)
        self._set_decryptor(None)

        dlg = QMessageBox(self)
        dlg.setWindowTitle("Cert unlock")

        if self.file_location is None:
            if self.ui.cards.currentData() is None:
                dlg.setText("You need to select one of the options.")
                self.on_dialog(dlg)
                return
            else:
                return self.unlock_card_clicked()

        if not exists(self.file_location):
            dlg.setText(f"File {self.file_location} does not exist!")
            self.on_dialog(dlg)
            return

        certs = Cert.split_file(self.file_location)

        if len(certs) > 1:
            dlg.setText("More than one certificate in the file!")
            self.on_dialog(dlg)
            return

        if len(certs) == 0:
            dlg.setText("No certificates found in that file!")
            self.on_dialog(dlg)
            return

        cert = certs[0]
        if not cert.has_secret_keys:
            dlg.setText(
                "Certificate doesn't contain secret parts. Please use --export-secret-keys instead of --export!"
            )
            self.on_dialog(dlg)
            return

        self.ui.key_fpr.setText(cert.fingerprint)
        self.ui.web_op_url.setText(self.ui.web_op_url.text() + cert.fingerprint)

        dlg.setText("Cert unlock succeeded!")

        secrets = cert.secrets
        try:
            self._set_signer(secrets.signer(None))
            self._set_certifier(secrets.certifier(None))
            self._set_decryptor(secrets.decryptor(None))
        except Exception as e:
            # some keys couldn't be unlocked, try asking the user for password
            pass

        if self.signer is not None:
            # signer is enough to proceed to next screen
            self.ui.pages.setCurrentIndex(1)
            return

        password = self.ask_for_password("Password:")
        if password is None:
            return
        if len(password) == 0:
            password = None
        try:
            signer = secrets.signer(password)
            self._set_signer(signer)

            self.ui.pages.setCurrentIndex(1)
        except Exception as e:
            dlg.setText("Cert unlock failed: " + str(e))
            self.on_dialog(dlg)
            return

        try:
            self._set_certifier(secrets.certifier(password))
        except Exception as e:
            dlg.setText("Cert unlock failed: " + str(e))
            self.on_dialog(dlg)
            return

        try:
            self._set_decryptor(secrets.decryptor(password))
        except Exception as e:
            dlg.setText("Cert unlock failed: " + str(e))
            self.on_dialog(dlg)

    def on_dialog(self, dlg):
        dlg.exec()

    def _set_signer(self, signer):
        self.signer = signer
        self.ui.sign_data.setEnabled(signer != None)
        self.ui.start_listen.setEnabled(signer != None)

    def _set_decryptor(self, decryptor):
        self.decryptor = decryptor
        self.ui.decrypt_data.setEnabled(decryptor != None)
        self.ui.start_listen.setEnabled(decryptor != None)

    def _set_certifier(self, certifier):
        self.certifier = certifier
        self.ui.sign_notations.setEnabled(certifier != None)
        self.ui.add_forge.setEnabled(certifier != None)

    def start_listen_clicked(self):
        httpServer = QHttpServer()
        global win
        win = self

        httpServer.route("/", route)
        httpServer.route("/decrypt", route)
        httpServer.route("/sign", route)
        httpServer.afterRequest(after_request)
        httpServer.listen(QHostAddress.LocalHost, 19841)

        self.server = httpServer

        self.ui.start_listen.setEnabled(False)
        self.ui.expose_label.setText("Waiting for requests...")

    def cert_unlock_succeeded(self):
        return self.ui.pages.currentIndex() > 0

    def route(self, request):
        headers = request.headers()
        origin = str(
            next(filter(lambda x: x[0] == QByteArray(b"Origin"), headers))[1], "utf-8"
        )
        print(f"Origin = {origin}")
        print(f"url = {request.url()}, body = {request.body()}")
        path = request.url().path()
        if f" {origin} " in f" {self.ui.allowed_domains.text()} ":
            if path == "/":
                return "ok"
            elif path == "/decrypt":
                body = request.body()
                self.tray_icon.showMessage("Private Key Store", f"Decrypting: {body}")
                decrypted = decrypt(decryptor=self.decryptor, bytes=bytes(body))

                return decrypted.bytes.decode("utf8")
            elif path == "/sign":
                body = request.body()
                self.tray_icon.showMessage("Private Key Store", f"Signing: {body}")
                signed = sign(self.signer, bytes(body))

                return signed.decode("utf8").replace("SIGNATURE", "MESSAGE")
            else:
                raise ValueError("Invalid path")
        else:
            raise ValueError("Not authorized")


def main():
    app = QApplication(sys.argv)

    win = Window()
    win.show()

    sys.exit(app.exec())


if __name__ == "__main__":
    main()
