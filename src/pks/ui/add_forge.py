# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'add_forge.ui'
##
## Created by: Qt User Interface Compiler version 6.6.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QDialog, QLabel, QLineEdit,
    QPushButton, QSizePolicy, QWidget)

class Ui_AddForgeProof(object):
    def setupUi(self, AddForgeProof):
        if not AddForgeProof.objectName():
            AddForgeProof.setObjectName(u"AddForgeProof")
        AddForgeProof.resize(400, 349)
        self.label = QLabel(AddForgeProof)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(20, 10, 361, 21))
        self.label_4 = QLabel(AddForgeProof)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setGeometry(QRect(20, 100, 71, 20))
        self.instance = QLineEdit(AddForgeProof)
        self.instance.setObjectName(u"instance")
        self.instance.setGeometry(QRect(110, 100, 251, 25))
        self.instance.setReadOnly(False)
        self.label_5 = QLabel(AddForgeProof)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setGeometry(QRect(20, 140, 71, 21))
        self.account = QLineEdit(AddForgeProof)
        self.account.setObjectName(u"account")
        self.account.setGeometry(QRect(110, 140, 251, 25))
        self.account.setReadOnly(False)
        self.authorize = QPushButton(AddForgeProof)
        self.authorize.setObjectName(u"authorize")
        self.authorize.setGeometry(QRect(280, 180, 80, 25))
        self.status = QLabel(AddForgeProof)
        self.status.setObjectName(u"status")
        self.status.setGeometry(QRect(30, 210, 311, 16))
        self.sign_proof_btn = QPushButton(AddForgeProof)
        self.sign_proof_btn.setObjectName(u"sign_proof_btn")
        self.sign_proof_btn.setEnabled(False)
        self.sign_proof_btn.setGeometry(QRect(280, 230, 80, 25))
        self.upload_btn = QPushButton(AddForgeProof)
        self.upload_btn.setObjectName(u"upload_btn")
        self.upload_btn.setEnabled(False)
        self.upload_btn.setGeometry(QRect(280, 270, 80, 25))
        self.label_6 = QLabel(AddForgeProof)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setGeometry(QRect(20, 60, 71, 20))
        self.fingerprint = QLineEdit(AddForgeProof)
        self.fingerprint.setObjectName(u"fingerprint")
        self.fingerprint.setEnabled(False)
        self.fingerprint.setGeometry(QRect(110, 60, 251, 25))
        self.fingerprint.setReadOnly(False)

        self.retranslateUi(AddForgeProof)

        QMetaObject.connectSlotsByName(AddForgeProof)
    # setupUi

    def retranslateUi(self, AddForgeProof):
        AddForgeProof.setWindowTitle(QCoreApplication.translate("AddForgeProof", u"Dialog", None))
        self.label.setText(QCoreApplication.translate("AddForgeProof", u"Add a new proof of your account on Forgejo / Gitea / Codeberg.", None))
        self.label_4.setText(QCoreApplication.translate("AddForgeProof", u"Instance", None))
        self.instance.setText(QCoreApplication.translate("AddForgeProof", u"codeberg.org", None))
        self.label_5.setText(QCoreApplication.translate("AddForgeProof", u"Account", None))
        self.authorize.setText(QCoreApplication.translate("AddForgeProof", u"Authorize", None))
        self.status.setText(QCoreApplication.translate("AddForgeProof", u"Waiting for authorization...", None))
        self.sign_proof_btn.setText(QCoreApplication.translate("AddForgeProof", u"Sign proof", None))
        self.upload_btn.setText(QCoreApplication.translate("AddForgeProof", u"Upload", None))
        self.label_6.setText(QCoreApplication.translate("AddForgeProof", u"Fingerprint", None))
        self.fingerprint.setText("")
    # retranslateUi

