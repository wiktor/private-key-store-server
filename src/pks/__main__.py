import sys

from PySide6.QtWidgets import (
    QApplication,
    QDialog,
    QMainWindow,
    QPushButton,
    QFileDialog,
    QMessageBox,
)

# from PySide6.QtHttpServer import QHttpServer
from PySide6.QtNetwork import QHostAddress, QTcpServer
from PySide6.QtCore import QByteArray

from pks.ui.main import Ui_Main
from pks.proof import AddProofWindow

from pysequoia import Cert, Card, sign, decrypt, Notation
from pks.ui.add_forge import Ui_AddForgeProof
from os.path import exists
from pks.main import Window


if __name__ == "__main__":
    app = QApplication(sys.argv)

    win = Window()
    win.show()

    sys.exit(app.exec())
