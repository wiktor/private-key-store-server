from PySide6.QtWidgets import (
    QApplication,
    QDialog,
    QWidget,
    QPushButton,
    QFileDialog,
    QMessageBox,
    QVBoxLayout,
    QLabel,
    QDialog,
)

from PySide6.QtGui import QWindow
from PySide6.QtCore import QTimer

from random import randint
from pks.ui.add_forge import Ui_AddForgeProof

from http.client import HTTPSConnection
import webbrowser
from pysequoia import Notation
import json


class AddProofWindow(QDialog):
    def __init__(self, cert, certifier):
        super().__init__()
        self.cert = cert
        self.certifier = certifier

        self.ui = Ui_AddForgeProof()
        self.ui.setupUi(self)
        self.ui.authorize.clicked.connect(self.authorize_clicked)
        self.ui.sign_proof_btn.clicked.connect(self.sign_proof_clicked)
        self.ui.upload_btn.clicked.connect(self.upload_clicked)

        self.ui.fingerprint.setText(cert.fingerprint)

        self.timer = QTimer()
        self.timer.timeout.connect(self.update)

    def authorize_clicked(self):
        instance = self.ui.instance.text()
        fingerprint = self.ui.fingerprint.text()
        account = self.ui.account.text()

        if instance == "" or fingerprint == "" or account == "":
            dlg = QMessageBox(self)
            dlg.setWindowTitle("Authorize")
            dlg.setText("Please fill in all fields!")
            dlg.exec()
        else:
            webbrowser.open(
                f"https://arm.services.metacode.biz/gitea-oauth/?instance=https://{instance}&fingerprint={fingerprint}"
            )
            self.ui.status.setText("Checking authorization...")
            self.timer.start(5000)

    def update(self):
        instance = self.ui.instance.text()
        conn = HTTPSConnection(instance)

        account = self.ui.account.text()
        conn.request("GET", f"/api/v1/repos/{account}/gitea_proof")

        resp = conn.getresponse()

        if resp.status == 200:
            self.timer.stop()
            self.ui.status.setText("Proof is present.")
            self.ui.sign_proof_btn.setEnabled(True)

        conn.close()

    def sign_proof_clicked(self):
        instance = self.ui.instance.text()
        account = self.ui.account.text()
        cert = self.cert
        notations = cert.user_ids[0].notations
        notations.append(
            Notation("proof@ariadne.id", f"https://{instance}/{account}/gitea_proof")
        )
        cert = cert.set_notations(self.certifier, notations)
        self.cert = cert
        self.ui.status.setText("Notations added to the certificate.")
        self.ui.upload_btn.setEnabled(True)

    def upload_clicked(self):
        conn = HTTPSConnection("keys.openpgp.org")
        conn.request(
            "POST",
            "/vks/v1/upload",
            json.dumps({"keytext": f"{self.cert}"}),
            {"Content-Type": "application/json"},
        )
        content = conn.getresponse()
        if content.status == 200:
            response = json.load(content)
            unverified_addresses = []
            for address, status in response["status"].items():
                if status == "unpublished":
                    unverified_addresses.append(address)
            if len(unverified_addresses) > 0:
                conn2 = HTTPSConnection("keys.openpgp.org")
                conn2.request(
                    "POST",
                    "/vks/v1/request-verify",
                    json.dumps(
                        {
                            "token": response["token"],
                            "addresses": unverified_addresses,
                            "locale": ["en_US", "de_DE"],
                        }
                    ),
                    {"Content-Type": "application/json"},
                )
                content2 = conn2.getresponse()
                conn2.close()
                self.ui.status.setText(
                    "Unverified addresses found, check your e-mail inbox!"
                )

            else:
                self.ui.status.setText("Certificate updated!")
        else:
            self.ui.status.setText("Certificate uploaded failed.")

        conn.close()
