from pks.main import Window
from PySide6 import QtCore
from pysequoia import Cert
from http.client import HTTPConnection
from threading import Thread
import subprocess
import tempfile

dlg = None


def on_dialog(dialog):
    global dlg
    dlg = dialog


def on_ask_for_password(text):
    return ""


def test_single_cert(qtbot, monkeypatch):
    cert = Cert.generate("testing cert")
    with open("/tmp/key.asc", "w") as f:
        f.write(str(cert.secrets))
    window = Window()
    qtbot.addWidget(window)
    window.file_location = "/tmp/key.asc"

    monkeypatch.setattr(window, "on_dialog", on_dialog)
    monkeypatch.setattr(window, "ask_for_password", on_ask_for_password)

    qtbot.mouseClick(window.ui.unlock_any, QtCore.Qt.MouseButton.LeftButton)
    assert window.cert_unlock_succeeded()


def test_no_secrets_cert(qtbot, monkeypatch):
    cert = Cert.generate("testing cert")
    with open("/tmp/key.asc", "w") as f:
        f.write(str(cert))  # not `.secrets`
    window = Window()
    qtbot.addWidget(window)
    window.file_location = "/tmp/key.asc"

    monkeypatch.setattr(window, "on_dialog", on_dialog)
    monkeypatch.setattr(window, "ask_for_password", on_ask_for_password)

    qtbot.mouseClick(window.ui.unlock_any, QtCore.Qt.MouseButton.LeftButton)
    assert dlg is not None
    assert (
        dlg.text()
        == "Certificate doesn't contain secret parts. Please use --export-secret-keys instead of --export!"
    )


def test_multiple_certs(qtbot, monkeypatch):
    cert = Cert.generate("testing cert")
    with open("/tmp/key.asc", "w") as f:
        cert = str(cert.secrets)
        f.write(cert)
        f.write(cert)
        f.write(cert)

    window = Window()
    qtbot.addWidget(window)
    window.file_location = "/tmp/key.asc"

    monkeypatch.setattr(window, "on_dialog", on_dialog)
    monkeypatch.setattr(window, "ask_for_password", on_ask_for_password)

    qtbot.mouseClick(window.ui.unlock_any, QtCore.Qt.MouseButton.LeftButton)
    assert dlg is not None
    assert dlg.text() == "More than one certificate in the file!"


def test_no_file(qtbot, monkeypatch):
    window = Window()
    qtbot.addWidget(window)
    window.file_location = ""

    monkeypatch.setattr(window, "on_dialog", on_dialog)
    monkeypatch.setattr(window, "ask_for_password", on_ask_for_password)

    qtbot.mouseClick(window.ui.unlock_any, QtCore.Qt.MouseButton.LeftButton)
    assert dlg is not None
    assert dlg.text() == "File  does not exist!"


def test_no_cert(qtbot, monkeypatch):
    window = Window()
    qtbot.addWidget(window)
    window.file_location = "/dev/null"

    monkeypatch.setattr(window, "on_dialog", on_dialog)
    monkeypatch.setattr(window, "ask_for_password", on_ask_for_password)

    qtbot.mouseClick(window.ui.unlock_any, QtCore.Qt.MouseButton.LeftButton)
    assert dlg is not None
    assert dlg.text() == "No certificates found in that file!"


def test_start_server(qtbot, monkeypatch):
    cert = Cert.generate("testing cert")
    with open("/tmp/key.asc", "w") as f:
        f.write(str(cert.secrets))

    window = Window()
    qtbot.addWidget(window)
    window.file_location = "/tmp/key.asc"

    monkeypatch.setattr(window, "on_dialog", on_dialog)
    monkeypatch.setattr(window, "ask_for_password", on_ask_for_password)

    qtbot.mouseClick(window.ui.unlock_any, QtCore.Qt.MouseButton.LeftButton)
    assert window.cert_unlock_succeeded()

    qtbot.mouseClick(window.ui.start_listen, QtCore.Qt.MouseButton.LeftButton)

    conn = HTTPConnection("127.0.0.1", 19841)

    conn.request("GET", "/", None, headers={"Origin": "http://localhost:8076"})

    resp = None

    def get_response_in_thread():
        nonlocal resp
        resp = conn.getresponse()

    t = Thread(target=get_response_in_thread)
    t.start()
    qtbot.waitUntil(lambda: resp is not None)
    assert resp.status == 200
    window.server = None
    conn.close()


def test_start_server_concurrently(qtbot, monkeypatch):
    cert = Cert.generate("testing cert")
    with open("/tmp/key.asc", "w") as f:
        f.write(str(cert.secrets))

    window = Window()
    qtbot.addWidget(window)
    window.file_location = "/tmp/key.asc"

    monkeypatch.setattr(window, "on_dialog", on_dialog)
    monkeypatch.setattr(window, "ask_for_password", on_ask_for_password)

    qtbot.mouseClick(window.ui.unlock_any, QtCore.Qt.MouseButton.LeftButton)
    assert window.cert_unlock_succeeded()

    qtbot.mouseClick(window.ui.start_listen, QtCore.Qt.MouseButton.LeftButton)

    conn = HTTPConnection("127.0.0.1", 19841)

    conn.request("GET", "/", None, headers={"Origin": "http://localhost:8076"})

    resp = None

    def get_response_in_thread():
        nonlocal resp
        resp = conn.getresponse()

    t = Thread(target=get_response_in_thread)
    t.start()
    qtbot.waitUntil(lambda: resp is not None)
    assert resp.status == 200
    window.server = None
    conn.close()


def test_server_decryption(qtbot, monkeypatch):
    cert = Cert.generate("testing cert")
    with open("/tmp/key.asc", "w") as f:
        f.write(str(cert.secrets))

    with open("/tmp/message.txt", "w") as f:
        f.write("this is a sample message")

    env = {"GNUPGHOME": tempfile.mkdtemp()}

    # encrypt the file using GnuPG
    assert subprocess.run(["gpg", "--import", "/tmp/key.asc"], env=env).returncode == 0
    encrypted = subprocess.run(
        [
            "gpg",
            "--batch",
            "--yes",
            "--output",
            "-",
            "--encrypt",
            "--trust-model",
            "always",
            "--armor",
            "--recipient",
            cert.fingerprint,
            "/tmp/message.txt",
        ],
        env=env,
        capture_output=True,
    )
    assert encrypted.returncode == 0

    window = Window()
    qtbot.addWidget(window)
    window.file_location = "/tmp/key.asc"

    monkeypatch.setattr(window, "on_dialog", on_dialog)
    monkeypatch.setattr(window, "ask_for_password", on_ask_for_password)

    qtbot.mouseClick(window.ui.unlock_any, QtCore.Qt.MouseButton.LeftButton)
    assert window.cert_unlock_succeeded()

    qtbot.mouseClick(window.ui.start_listen, QtCore.Qt.MouseButton.LeftButton)

    conn = HTTPConnection("127.0.0.1", 19841)

    data = encrypted.stdout.decode("utf8")
    assert len(data) > 0
    conn.request("POST", "/decrypt", data, headers={"Origin": "http://localhost:8076"})

    resp = None

    def get_response_in_thread():
        nonlocal resp
        resp = conn.getresponse()

    t = Thread(target=get_response_in_thread)
    t.start()
    qtbot.waitUntil(lambda: resp is not None)
    assert resp.status == 200
    body = resp.read()
    assert body == b"this is a sample message"
    window.server = None
    conn.close()


def test_server_signing(qtbot, monkeypatch):
    cert = Cert.generate("testing cert")
    with open("/tmp/signing-key.asc", "w") as f:
        f.write(str(cert.secrets))

    window = Window()
    qtbot.addWidget(window)
    window.file_location = "/tmp/signing-key.asc"

    monkeypatch.setattr(window, "on_dialog", on_dialog)
    monkeypatch.setattr(window, "ask_for_password", on_ask_for_password)

    qtbot.mouseClick(window.ui.unlock_any, QtCore.Qt.MouseButton.LeftButton)
    assert window.cert_unlock_succeeded()

    qtbot.mouseClick(window.ui.start_listen, QtCore.Qt.MouseButton.LeftButton)

    conn = HTTPConnection("127.0.0.1", 19841)

    data = "this is a sample message"
    assert len(data) > 0
    conn.request("POST", "/sign", data, headers={"Origin": "http://localhost:8076"})

    resp = None

    def get_response_in_thread():
        nonlocal resp
        resp = conn.getresponse()

    t = Thread(target=get_response_in_thread)
    t.start()
    qtbot.waitUntil(lambda: resp is not None)
    assert resp.status == 200
    body = resp.read()
    with open("/tmp/signed-message.txt", "wb") as f:
        f.write(body)
    window.server = None
    conn.close()

    env = {"GNUPGHOME": tempfile.mkdtemp()}

    # verify the file using GnuPG
    assert (
        subprocess.run(["gpg", "--import", "/tmp/signing-key.asc"], env=env).returncode
        == 0
    )
    verified = subprocess.run(
        [
            "gpg",
            "--batch",
            "--yes",
            "--trust-model",
            "always",
            "--output",
            "-",
            "--verify",
            "/tmp/signed-message.txt",
        ],
        env=env,
        capture_output=True,
    )
    assert verified.returncode == 0
    assert verified.stdout.decode("utf8") == data
